import unittest
import game_of_life as game
from game_of_life import Cell


class TestGameOfLifeRules(unittest.TestCase):
    """ test cases for the game and rules """

    def setUp(self):
        self.sut = game.GameOfLife(10, 10)

    def test_count_of_neighbours(self):
        self.sut.setCellAlive(Cell(1, 1))
        result = self.sut.countOfNeighbours(Cell(1, 2))
        self.assertEqual(result, 1)

    def test_newLife_when_the_cell_has_one_neighbour_should_be_false(self):
        """ rules for new life """
        result = self.sut.newLife(True, 1)
        self.assertFalse(result)

    def test_newLife_when_the_cell_has_two_neighbour_should_be_true(self):
        """ do we need a doc string with a method name like this? """
        result = self.sut.newLife(True, 2)
        self.assertTrue(result)

    def test_newLife_when_the_cell_has_three_neighbour_should_be_true(self):
        result = self.sut.newLife(True, 3)
        self.assertTrue(result)

    def test_newLife_when_the_cell_has_four_neighbour_should_be_false(self):
        result = self.sut.newLife(True, 4)
        self.assertFalse(result)

    def test_isAlive_should_be_true(self):
        """ does this work? """
        self.sut.board[1][1] = True
        result = self.sut.isAlive(Cell(x=1, y=1))
        self.assertTrue(result)

    def test_isAlive_should_be_false(self):
        """ does this work? """
        self.sut.board[1][1] = True
        result = self.sut.isAlive(Cell(x=3, y=1))
        self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()


import game_of_life as game
import pygame
from random import *

""" Run the game and show the results on screen """

pygame.init()
screen = pygame.display.set_mode((800, 800))
pygame.display.set_caption("conways game of life")
clock = pygame.time.Clock()

done = False
green = (0, 255, 0)
white = (255, 200, 255)
black = (13, 15, 19)
xMax = 80
yMax = 80

gameOfLife = game.GameOfLife(80,80)

def rndColorGenerator(g):
    e = True
    while e:
        red = randint(0, 255)
        green = randint(0, 255)
        blue = randint(0, 255)
        yield (red, green, blue)

gen = rndColorGenerator(100)

def drawCell(board, colorFunc):
    """ colour is a function!!
    """
    for x in range(xMax):
        for y in range(yMax):
            if (board[x][y]):
                pygame.draw.circle(screen, colorFunc(), (10 * x, 10 * y), 5, 0)

def drawLife(board):
    """ Life is magical!! and so are the colours  """
    drawCell(board,lambda :  gen.__next__())

def drawDeath(board):
    """ black is the colour of death! """
    drawCell(board, lambda : black)


followMouse = False
while not done:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            gameOfLife.randomWorldGenerator()

        if event.type == pygame.MOUSEBUTTONDOWN:
            (mouseX, mouseY) = pygame.mouse.get_pos()
            followMouse = True 

        if event.type == pygame.MOUSEBUTTONUP:
            followMouse = False 

        if followMouse:
            (mouseX, mouseY) = pygame.mouse.get_pos()




    board = gameOfLife.board
    drawLife(board)

    gameOfLife.play()
    pygame.display.update()

    # clear the previous life ready for the new board
    drawDeath(board)

    clock.tick(9)

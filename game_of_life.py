from collections import namedtuple
from random import *

Cell = namedtuple('Cell', ['x', 'y'], verbose = False)
CellState = namedtuple('CellState', ['cell','isAlive','countOfNeighbours' ], verbose = False)

class GameOfLife:
    """ the GameOfLife, used to store the state of the game and holds the 
        rules for the game.
    """
    def __init__(self,xSize, ySize):
        """ Setup the game with an empty board """
        self.xSize = xSize
        self.ySize = ySize
        self.board = self.makeEmptyBoard()

    def setCellAlive(self,cell):
        self.board[cell.x][cell.y] = True

    def makeEmptyBoard(self):
        """ this is a list and each one contains a list row and col
            every cell has a value True or False, to indicate life.
            You can think of this as a muliti-dimentional array.
        """
        board = []
        for x in range(self.xSize):
            row = []
            for y in range(self.ySize):
                row.append(False)
            board.append(row)
        return board

    def randomWorldGenerator(self):
        for x in range(self.xSize):
            for y in range(self.ySize):
                if (randint(0, 1) == 1):
                    self.setCellAlive(Cell(x=x,y=y))

    def isAlive(self, cell):
        """ Return true if this position (vector) on the board has life .
            The problem with boundries, this function needs to know the world
            boundries. As we will get an error if we try to check anything
            outside that area.
            The world should really be an object, which could return it's size.
        """
        if (cell.x < 0 or cell.y <0 or 
                cell.x >= self.xSize or cell.y >= self.xSize):
            return False

        return self.board[cell.x][cell.y]
    
    def countOfNeighbours(self, cell):
        """ Define the relative position of each neighbour 
            loop each neighbour and check the world to count live cells """
        neighbours = {(-1, -1), (0, -1), (1, -1),
                    (-1, 0), (1, 0),
                    (-1, 1), (0, 1), (1, 1)}

        count = 0
        for neighbourCell in neighbours:
            cx = cell.x + neighbourCell[0]
            cy = cell.y + neighbourCell[1]
            if (self.isAlive( Cell(x=cx,y= cy) ) ):
                count = count + 1
        return count

    def newLife(self, cellAlive, neighbourCount):
        """ the rules for new life """
        if (cellAlive == True and (neighbourCount == 2 or neighbourCount == 3)):
            return True

        if (cellAlive == False and neighbourCount == 3):
            return True

    def worldIterator(self):
        """ Simple iterator to return each x,y of the board """
        for x in range(0, self.xSize):
            for y in range(0, self.ySize):
                cell = Cell(x, y)
                isAlive = self.isAlive(cell)
                countOfNeighbours = self.countOfNeighbours(cell)
                yield CellState(Cell(x,y),  isAlive, countOfNeighbours)

    def play(self):
        newBoard = self.makeEmptyBoard() 

        for cell in self.worldIterator():
            if (self.newLife( cell.isAlive, cell.countOfNeighbours) ):
                newBoard[cell.cell.x][cell.cell.y] = True

        self.board = newBoard
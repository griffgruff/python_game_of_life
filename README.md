## Conway's Game of Life

Just for fun an implementations of this classic.  I've used pygame to display the game!

Runs under python 3.5 and probably other versions as well. It does require **pygame** to be installed.

## Start the game

Run the game with :

>python app.py

To create a random world/board press the space bar.

The game rules have been implemented in game_of_life.py. A class called GameOfLife is where all the action happens. It's pretty much a state machine.

Test cases are in test_cases.py!!

>python test_cases.py

## Notes

The game starts with a muliti-dimentional array of a fixed size. Each cell in the array is set to False, to indicate no life.

**Cell** and **namedtuple**, a namedtuple is used to represent the Cell. It's just a tuple with x and y values.

### Generators

The worldIterator is a generator, it allows the game to iterate over the board. It also returns the current cell and the state of the cell with the count of neighbours.

### New Game

After each iteration the board must be updated with a new board, with the new state.


### Random Colour Generator and lambda

When drawing life, every life cell is a different colour, so in-order to have just one method of drawing life. The system calls the colourFunc to determine what the colour of the next cell should be.

Lambdas are pretty good for this purpose, and that is what I have used.

```python
    def drawCell(board, colorFun):
        .....

    drawCell(board,lambda :  gen.__next__())
```
